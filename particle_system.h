#ifndef PARTICLE_SYSTEM_H
#define PARTICLE_SYSTEM_H

#include <vector>
#include <memory>

#include "aabb.h"
#include "particle.h"
#include "solver/solver.h"

class ParticleSystem
{
public:
  ParticleSystem(int particle_count, const AABB &bound);
  virtual ~ParticleSystem();

  template<class T> inline
  void set_solver()
  {
    solver_ = std::unique_ptr<Solver>(new T(particles_, bound_));
  }

  void init_particles();
  const std::vector<Particle> & get_particles() const;
  void update();

protected:
  void bound_collide();

  std::vector<Particle> particles_;
  AABB bound_;
  std::unique_ptr<Solver> solver_;
};

#endif // PARTICLE_SYSTEM_H
