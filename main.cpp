#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>

#include "particle_system.h"
#include "solver/bruteforce_solver.h"
#include "solver/sap_solver.h"
#include "solver/grid_solver.h"

int main()
{
  ParticleSystem ps(5000, AABB(0.0f, 0.0f, 600.0f, 600.0f));
  ps.set_solver<SAPSolver>();

  sf::ContextSettings settings;
  settings.depthBits = 24;
  settings.stencilBits = 0;
  settings.antialiasingLevel = 0;
  settings.majorVersion = 1;
  settings.minorVersion = 4;

  sf::RenderWindow window(sf::VideoMode(600, 600), "ParticleSystem",
                          sf::Style::Default, settings);
  window.setVerticalSyncEnabled(true);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0.0, 600.0, 600.0, 0.0, 0.0, 1.0);

  while (window.isOpen())
  {
    sf::Event event;
    while (window.pollEvent(event))
    {
      if (event.type == sf::Event::Closed)
      {
        window.close();
      }
      else if (event.type == sf::Event::Resized)
      {
        glViewport(0, 0, event.size.width, event.size.height);
      }
      else if (event.type == sf::Event::KeyPressed)
      {
        if (event.key.code == sf::Keyboard::Num1)
        {
          ps.set_solver<BruteforceSolver>();
        }
        else if (event.key.code == sf::Keyboard::Num2)
        {
          ps.set_solver<SAPSolver>();
        }
        else if (event.key.code == sf::Keyboard::Num3)
        {
          ps.set_solver<GridSolver>();
        }
        else if (event.key.code == sf::Keyboard::Escape)
        {
          window.close();
        }
      }
    }

    glClear(GL_COLOR_BUFFER_BIT);

    glColor3f(1.0f, 1.0f, 1.0f);
    glBegin(GL_POINTS);
    for (auto &particle: ps.get_particles())
    {
      glVertex2f(particle.get_pos().x, particle.get_pos().y);
    }
    glEnd();

    window.display();

    ps.update();
  }

  return 0;
}
