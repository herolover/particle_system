#include <algorithm>

#include "aabb.h"

AABB::AABB()
  : empty_(true)
{
}

AABB::AABB(float x1, float y1, float x2, float y2)
  : min_(x1, y1)
  , max_(x2, y2)
  , empty_(false)
{
}

void AABB::expand(const Vec2 &point)
{
  if (empty_)
  {
    empty_ = false;
    min_ = point;
    max_ = point;
  }
  else
  {
    min_.x = std::min(min_.x, point.x);
    min_.y = std::min(min_.y, point.y);

    max_.x = std::max(max_.x, point.x);
    max_.y = std::max(max_.y, point.y);
  }
}
