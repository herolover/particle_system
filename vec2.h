#ifndef VEC2_H
#define VEC2_H

#include <cmath>
#include <cstdlib>

template<class T> inline
T sqr(T value)
{
  return value * value;
}

class Vec2
{
public:
  float x, y;

  Vec2()
    : x(0.0f), y(0.0f)
  {
  }

  Vec2(float _x, float _y)
    : x(_x), y(_y)
  {
  }

  Vec2(const Vec2 &v)
    : x(v.x), y(v.y)
  {
  }

  inline Vec2 operator -() const
  {
    return Vec2(-x, -y);
  }

  inline Vec2 & operator = (const Vec2 &v)
  {
    x = v.x;
    y = v.y;

    return *this;
  }

  inline Vec2 & operator += (const Vec2 &v)
  {
    x += v.x;
    y += v.y;

    return *this;
  }

  inline Vec2 & operator -= (const Vec2 &v)
  {
    x -= v.x;
    y -= v.y;

    return *this;
  }

  inline Vec2 & operator *= (float scalar)
  {
    x *= scalar;
    y *= scalar;

    return *this;
  }

  inline Vec2 & operator /= (float scalar)
  {
    x /= scalar;
    y /= scalar;

    return *this;
  }

  inline void rand()
  {
    float angle = (std::rand() % 360) * M_PI / 180.0f;

    x = cos(angle);
    y = sin(angle);
  }

  inline float length() const
  {
    return sqrt(sqr(x) + sqr(y));
  }

  inline float quad_length() const
  {
    return sqr(x) + sqr(y);
  }

  inline Vec2 & normalize()
  {
    float length = this->length();
    x /= length;
    y /= length;

    return *this;
  }

  inline Vec2 & normalize(float length)
  {
    x /= length;
    y /= length;

    return *this;
  }

  inline Vec2 get_normalize() const
  {
    Vec2 res(*this);
    res /= this->length();

    return res;
  }

  inline Vec2 & to_zero()
  {
    x = 0.0f;
    y = 0.0f;

    return *this;
  }

  inline Vec2 left() const
  {
    return Vec2(y, -x);
  }

  inline Vec2 right() const
  {
    return Vec2(-y, x);
  }

  inline Vec2 & invert()
  {
    x = -x;
    y = -y;

    return *this;
  }
};

inline Vec2 operator + (const Vec2 &v1, const Vec2 &v2)
{
  return Vec2(v1.x + v2.x, v1.y + v2.y);
}

inline Vec2 operator - (const Vec2 &v1, const Vec2 &v2)
{
  return Vec2(v1.x - v2.x, v1.y - v2.y);
}

inline Vec2 operator * (const Vec2 &v, float scalar)
{
  return Vec2(v.x * scalar, v.y * scalar);
}

inline Vec2 operator * (float scalar, const Vec2 &v)
{
  return Vec2(scalar * v.x, scalar * v.y);
}

inline Vec2 operator * (const Vec2 &v1, const Vec2 &v2)
{
  return Vec2(v1.x * v2.x, v1.y * v2.y);
}

inline Vec2 operator / (const Vec2 &v, float scalar)
{
  return Vec2(v.x / scalar, v.y / scalar);
}

inline Vec2 operator / (float scalar, const Vec2 &v)
{
  return Vec2(scalar / v.x, scalar / v.y);
}

inline Vec2 operator / (const Vec2 &v1, const Vec2 &v2)
{
  return Vec2(v1.x / v2.x, v1.y / v2.y);
}

inline float operator & (const Vec2 &v1, const Vec2 &v2)
{
  return v1.x * v2.x + v1.y * v2.y;
}

inline float operator ^ (const Vec2 &v1, const Vec2 &v2)
{
  return v1.x * v2.y - v1.y * v2.x;
}

#endif // VEC2_H
