import qbs 1.0

Project {
    CppApplication {
        name: "particle_system"

        files: [
            "main.cpp",
            "particle.h",
            "particle_system.h",
            "particle_system.cpp",
            "aabb.h",
            "aabb.cpp",
            "vec2.h",
            "solver/solver.h",
            "solver/solver.cpp",
            "solver/bruteforce_solver.h",
            "solver/bruteforce_solver.cpp",
            "solver/sap_solver.h",
            "solver/sap_solver.cpp",
            "solver/grid_solver.h",
            "solver/grid_solver.cpp"
        ]

        cpp.cxxFlags: [
            "-std=c++11"
        ]

        cpp.staticLibraries: [
            "sfml-graphics",
            "sfml-window",
            "sfml-system",
            "GL"
        ]
    }
}
