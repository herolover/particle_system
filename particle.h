#ifndef PARTICLE_H
#define PARTICLE_H

#include "vec2.h"
#include "aabb.h"

class Particle
{
public:
  inline
  Particle()
    : radius_(2.0f)
  {
  }

  inline
  void push(const Vec2 &force)
  {
    pos_ += force;

    float max_speed = 3.0f;
    if ((pos_ - old_pos_).quad_length() > sqr(max_speed))
    {
      pos_ = old_pos_ + (pos_ - old_pos_).normalize() * max_speed;
    }

    bound_.circle_init(pos_, radius_);
  }

  inline
  void set_pos(const Vec2 &pos)
  {
    pos_ = pos;
    old_pos_ = pos;

    bound_.circle_init(pos_, radius_);
  }

  inline
  const Vec2 & get_pos() const
  {
    return pos_;
  }

  inline
  float get_radius() const
  {
    return radius_;
  }

  inline
  const AABB & get_bound() const
  {
    return bound_;
  }

  inline
  void update()
  {
    Vec2 tmp = pos_;
    pos_ += (pos_ - old_pos_) * 0.998f;
    old_pos_ = tmp;

    bound_.circle_init(pos_, radius_);
  }

private:
  Vec2 pos_;
  Vec2 old_pos_;
  float radius_;
  AABB bound_;
};

#endif // PARTICLE_H
