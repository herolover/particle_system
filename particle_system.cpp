#include "particle_system.h"

ParticleSystem::ParticleSystem(int particle_count,
                               const AABB &bound)
  : particles_(particle_count)
  , bound_(bound)
{
  init_particles();
}

ParticleSystem::~ParticleSystem()
{
}

void ParticleSystem::init_particles()
{
  for (auto &particle: particles_)
  {
    particle.set_pos(bound_.make_random_point());
  }
}

const std::vector<Particle> & ParticleSystem::get_particles() const
{
  return particles_;
}

void ParticleSystem::update()
{
  for (auto &particle: particles_)
  {
    particle.push(Vec2(0.0f, 0.003f));
    particle.update();
  }

  if (solver_ != nullptr)
  {
    solver_->update();
  }

  bound_collide();
}

void ParticleSystem::bound_collide()
{
  const float bounce = 1.1f;

  for (auto &particle: particles_)
  {
    if (particle.get_bound().get_min().x < bound_.get_min().x)
    {
      particle.push(Vec2(bound_.get_min().x - particle.get_bound().get_min().x, 0.0f) * bounce);
    }
    else if (particle.get_bound().get_max().x > bound_.get_max().x)
    {
      particle.push(Vec2(bound_.get_max().x - particle.get_bound().get_max().x, 0.0f) * bounce);
    }

    if (particle.get_bound().get_min().y < bound_.get_min().y)
    {
      particle.push(Vec2(0.0f, bound_.get_min().y - particle.get_bound().get_min().y) * bounce);
    }
    else if (particle.get_bound().get_max().y > bound_.get_max().y)
    {
      particle.push(Vec2(0.0f, bound_.get_max().y - particle.get_bound().get_max().y) * bounce);
    }
  }
}
