#include "grid_solver.h"

#include <algorithm>

GridSolver::GridSolver(std::vector<Particle> &particles, const AABB &bound)
  : Solver(particles, bound)
{
  grid_size_ = 10.0f;
  grid_width_ = bound.get_width() / grid_size_;
  grid_height_ = bound.get_height() / grid_size_;
  grid_size_ += 0.01f;

  grid_.resize(grid_width_ * grid_height_);

  place_particles();
}

void GridSolver::update()
{
}

void GridSolver::place_particles()
{
  for (auto &particle: particles_)
  {
    auto corners = particle.get_bound().get_corners();
    for (Vec2 &corner: corners)
    {
      int x = corner.x / grid_size_;
      int y = corner.y / grid_size_;

      grid_[x * grid_height_ + y].insert(&particle);
    }
  }
}
