#include "sap_solver.h"

#include <algorithm>

SAPSolver::SAPSolver(std::vector<Particle> &particles, const AABB &bound)
  : Solver(particles, bound)
{
}

void SAPSolver::update()
{
  std::sort(std::begin(particles_), std::end(particles_),
            [](const Particle &p1, const Particle &p2)
  {
    return p1.get_bound().get_min().x < p2.get_bound().get_min().x;
  });

  for (unsigned i = 0; i < particles_.size(); ++i)
  {
    for (unsigned j = i + 1; j < particles_.size(); ++j)
    {
      if (particles_[j].get_bound().get_min().x > particles_[i].get_bound().get_max().x)
      {
        break;
      }

      check_on_collide(particles_[i], particles_[j]);
    }
  }
}
