#ifndef SOLVER_BRUTEFORCE_H
#define	SOLVER_BRUTEFORCE_H

#include "solver.h"

class BruteforceSolver: public Solver
{
public:
  BruteforceSolver(std::vector<Particle> &particles, const AABB &bound);

  void update() final;
};

#endif
