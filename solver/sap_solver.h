#ifndef SOLVER_SAP_H
#define	SOLVER_SAP_H

#include "solver.h"

class SAPSolver: public Solver
{
public:
  SAPSolver(std::vector<Particle> &particles, const AABB &bound);

  void update() final;
};

#endif
