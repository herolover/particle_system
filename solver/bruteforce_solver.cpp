#include "bruteforce_solver.h"

BruteforceSolver::BruteforceSolver(std::vector<Particle> &particles,
                                   const AABB &bound)
  : Solver(particles, bound)
{
}

void BruteforceSolver::update()
{
  for (unsigned i = 0; i < particles_.size(); ++i)
  {
    for (unsigned j = i + 1; j < particles_.size(); ++j)
    {
      check_on_collide(particles_[i], particles_[j]);
    }
  }
}
