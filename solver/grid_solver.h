#ifndef GRID_SOLVER_H
#define GRID_SOLVER_H

#include <vector>
#include <unordered_set>

#include "../aabb.h"
#include "../particle.h"
#include "solver.h"

class GridSolver: public Solver
{
public:
  GridSolver(std::vector<Particle> &particles, const AABB &bound);

  void update() final;

private:
  void place_particles();

  std::vector<std::unordered_set<Particle *>> grid_;
  int grid_width_;
  int grid_height_;
  float grid_size_;
};

#endif // GRID_SOLVER_H
