#ifndef SOLVER_H
#define	SOLVER_H

#include <vector>

#include "../particle.h"
#include "../aabb.h"

class Solver
{
public:
  Solver(std::vector<Particle> &particles, const AABB &bound);
  virtual ~Solver();

  virtual void update() = 0;

  static
  void check_on_collide(Particle &p1, Particle &p2);

protected:
  std::vector<Particle> &particles_;
  const AABB &bound_;
};


#endif
