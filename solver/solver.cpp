#include "solver.h"

void Solver::check_on_collide(Particle &p1, Particle &p2)
{
  Vec2 delta = p1.get_pos() - p2.get_pos();
  float distance = delta.length();
  float min_distance = p1.get_radius() + p2.get_radius();

  if (distance < min_distance)
  {
    if (distance != 0.0f)
    {
      float penetration = min_distance - distance;
      delta *= penetration * 0.4f / distance;
    }
    else
    {
      delta.rand();
      delta *= min_distance * 0.5f;
    }

    p1.push(delta);
    p2.push(-delta);
  }
}

Solver::Solver(std::vector<Particle> &particles, const AABB &bound)
  : particles_(particles)
  , bound_(bound)
{
}

Solver::~Solver()
{
}
