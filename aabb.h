#ifndef AABB_H
#define	AABB_H

#include <cstdlib>

#include "vec2.h"

class AABB
{
public:
  AABB();
  AABB(float x1, float y1, float x2, float y2);

  inline
  const Vec2 & get_min() const
  {
    return min_;
  }

  inline
  const Vec2 & get_max() const
  {
    return max_;
  }

  inline
  float get_width() const
  {
    return max_.x - min_.x;
  }

  inline
  float get_height() const
  {
    return max_.y - min_.y;
  }

  std::vector<Vec2> get_corners() const
  {
    return std::vector<Vec2>({min_, Vec2(min_.x, max_.y),
                              max_, Vec2(max_.x, min_.y)});
  }

  inline
  void clear()
  {
    empty_ = true;
  }

  inline
  void circle_init(const Vec2 &pos, float radius)
  {
    empty_ = false;
    min_ = pos - Vec2(radius, radius);
    max_ = pos + Vec2(radius, radius);
  }

  void expand(const Vec2 &point);

  inline
  Vec2 make_random_point() const
  {
    return Vec2(min_.x + (std::rand() % (int)get_width()),
                min_.y + (std::rand() % (int)get_height()));
  }

private:
  Vec2 min_;
  Vec2 max_;
  bool empty_;
};

#endif
